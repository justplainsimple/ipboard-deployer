require 'net/ssh'
require 'net/scp'
require './lib/ipboard/server'
require './lib/ipboard/deployer'
require 'benchmark'
require 'yaml'

if __FILE__ == $0
  elapsed = 0   # seconds

  begin
    elapsed = Benchmark.realtime do
      CONFIG_FILE = "deployer.yaml"
      TIMESTAMP = Time.now.utc

      raise("Missing configuration file.  Please create one from the #{CONFIG_FILE}.sample file.") unless File.exists?(CONFIG_FILE)

      servers = []
      config = YAML.load_file(CONFIG_FILE)["deployer"]

      config['servers'].each do |server|
        servers << IPBoard::Server.new(server['name'], server['hostname'], config['username'], config['deploy_dir'], config['nfs_share_dir'], config['web_dir'], server['ssh_port'])
      end

      servers.each do |server|
        deployer = IPBoard::Deployer.new(server, config['key_file'], TIMESTAMP)
        deployer.debug = config['debug']
        deployer.deploy!
      end
    end
  rescue RuntimeError => e
    puts "ERROR: #{e.message}"
  ensure
    puts "\nFinished in #{elapsed.ceil} seconds"
  end
end
