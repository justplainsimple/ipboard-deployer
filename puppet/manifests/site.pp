node default {
  include core
  include vim
}

node node1, node2 inherits default {
  motd::set_motd {
    "set_app_motd":
      content => "IPBoard :: Web Node"
  }

  include apache2
  include php5
  include php5-mysql
  include php5-gd
  include unzip

  apache2::create_vhost {
    "create_site":
      email => "scottb@ea.com",
      hostname => "ipboard.forum.ea.com",
      www_dir => "/var/www/forum"
  }

  exec { "change_web_dir_perms":
    command => "chmod 777 /var/www",
    path => "/usr/bin:/usr/sbin:/bin:/usr/local/bin",
    require => Package[apache2]
  }
}

node database inherits default {
  motd::set_motd {
    "set_app_motd":
      content => "IPBoard :: Database"
  }

  include mysql

  mysql::create_database {
    "create_db":
      name => "ipboard"
  }

  mysql::grant_access_to_db {
    "grant_node1_access":
      username => "root",
      password => "root",
      ip_address => "33.33.13.31";
    "grant_node2_access":
      username => "root",
      password => "root",
      ip_address => "33.33.13.32";
  }
}

node loadbalancer inherits default {
  motd::set_motd {
    "set_app_motd":
      content => "IPBoard :: Load Balancer"
  }

  include pound
}
