class php5-gd {
  package { "php5-gd":
    ensure => installed,
    require => Package[php5],
    notify => Service[apache2]
  }
}