class vim {
  package {
    "vim":
      ensure => installed
  }

  file {
    "/etc/vimrc":
      mode => 644,
      owner => root,
      group => root,
      content => template("vim/vimrc.erb")
  }
}