class tmux {
  package {
    "tmux":
      ensure => installed
  }

  file {
    "/etc/tmux.conf":
      owner => root,
      group => root,
      mode => 644,
      content => template("tmux/tmux.conf.erb")
  }
}