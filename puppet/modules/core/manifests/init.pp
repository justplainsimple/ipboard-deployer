class core {
  group {
    "puppet":
      ensure => present
  }

  exec {
    "apt-update":
      command => "sudo apt-get update",
      require => Group[puppet]
  }
  Exec["apt-update"] -> Package <| |>

  file {
    "/opt/":
      ensure => directory,
      owner => root,
      group => root,
      mode => 755;

    "/opt/apps/":
      ensure => directory,
      owner => root,
      group => root,
      mode => 755,
      require => File["/opt/"];
  }

  Exec {
    path => ["/bin/", "/sbin/", "/usr/bin/", "/usr/sbin/", "/usr/local/bin/"]
  }
}