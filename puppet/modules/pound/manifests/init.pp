class pound {
  package { "pound":
    ensure => installed,
  }

  file { "/etc/pound/pound.cfg":
    ensure => file,
    owner => root,
    group => root,
    mode => 644,
    content => template("pound/pound.cfg.erb"),
    require => Package[pound],
    notify => Service[pound]
  }

  file { "/etc/default/pound":
    ensure => file,
    owner => root,
    group => root,
    mode => 644,
    content => template("pound/default.pound.erb"),
    require => Package[pound],
    notify => Service[pound]
  }

  service { "pound":
    enable => true,
    ensure => running,
    require => Package[pound]
  }
}