require 'net/ssh'
require 'net/scp'

module IPBoard
  class Deployer
    TIMESTAMP_DIRECTORY_FORMAT = "%Y%m%d%H%M%S"
    LATEST_SOFTWARE_VERSION = "3.3.4"

    attr_accessor :server, :key, :timestamp, :debug

    def initialize (server, key, timestamp)
      @server = server
      @key = key
      @timestamp = timestamp
      @debug = false
      @ssh = nil
    end

    def has_releases?
      # YAGNI
      @ssh.exec "ls #{@server.releases_dir} | grep -v 'current' | grep -v 'previous' | wc -l".chomp > 1
    end

    def deploy!
      print "Connecting to #{@server.name}..." if @debug
      ssh_connection do |ssh|
        puts "done." if @debug

        # create releases directory
        unless is_directory?(@server.releases_dir)
          within_ssh("Creating releases directory") do |ch|
            ch.exec "mkdir -p #{@server.releases_dir}"
          end
        end

        # create shared directory
        unless is_directory?(@server.shared_dir)
          within_ssh("Creating shared directory") do |ch|
            ch.exec "mkdir -p #{@server.shared_dir}"
          end
        end

        # create nfs/shared/config directory
        unless exists?(File.join(@server.nfs_share_dir, "config"))
          within_ssh("Creating nfs/shared/config directory") do |ch|
            ch.exec "mkdir -p #{@server.nfs_share_dir}/config"
          end
        end

        # copy the code to server
        software_name = "IPBoard-#{LATEST_SOFTWARE_VERSION}.zip"
        print "Pushing software to server..." if @debug
        ssh.scp.upload!("software/#{software_name}", "/tmp/")
        puts "done." if @debug

        within_ssh("Unzipping software on server") do |ch|
          ch.exec "unzip /tmp/#{software_name} -d /tmp/"
        end

        within_ssh("Moving software into #{next_release_dir}") do |ch|
          ch.exec "mv /tmp/board/upload #{next_release_dir}"
        end

        within_ssh("Cleaning up software in temporary location") do |ch|
          ch.exec "rm /tmp/#{software_name} && rm -rf /tmp/board"
        end

        # move [cache, hooks, uploads, public] directory to nfs/shared/
        # iff it doesn't exist.  Otherwise delete directory.
        %w{ cache hooks uploads public }.each do |dir|
          if exists?(File.join(@server.nfs_share_dir, dir))
            within_ssh("Deleting #{dir} from deploy dir") do |ch|
              ch.exec "rm -rf #{next_release_dir}/#{dir}"
            end
          else
            within_ssh("Moving #{dir} to NFS share") do |ch|
              ch.exec "mv #{next_release_dir}/#{dir} #{@server.nfs_share_dir}/"
            end
          end
        end

        # move the config file to nfs/shared/config
        if exists?(File.join(@server.nfs_share_dir, "config", "conf_global.php"))
          if exists?(File.join(next_release_dir, "conf_global.php"))
            within_ssh("Deleting base config file") do |ch|
              ch.exec "rm #{next_release_dir}/conf_global.php"
            end
          end
        else
          within_ssh("Moving config file to NFS share") do |ch|
            ch.exec "mv #{next_release_dir}/conf_global.dist.php #{@server.nfs_share_dir}/config/conf_global.php"
          end
        end

        # create symlink from nfs_share/[config,cache,hooks,uploads,public] 
        # to deploy_dir/shared/[config,cache,hooks,uploads,public]
        %w{ config cache hooks uploads public }.each do |dir|
          unless is_symlink?(File.join(@server.shared_dir, dir))
            within_ssh("Creating symlink for #{dir} file") do |ch|
              ch.exec "ln -s #{@server.nfs_share_dir}/#{dir} #{@server.shared_dir}/#{dir}"
            end
          end
        end

        # Link the global config file to the new release
        # - create symlink from shared/config/conf_global.php to deploy_dir/conf_global.php
        within_ssh("Creating symlink for config file") do |ch|
          ch.exec "ln -s #{@server.shared_dir}/config/conf_global.php #{next_release_dir}/conf_global.php"
        end

        # Link the shared directories to the new release
        # - create symlink from shared/[cache,hooks,uploads,public] 
        # to deploy_dir/[cache,hooks,uploads,public]
        %w{ cache hooks uploads public }.each do |dir|
          within_ssh("Creating symlink for #{dir} directory") do |ch|
            ch.exec "ln -s #{@server.shared_dir}/#{dir} #{next_release_dir}/#{dir}"
          end
        end

        # create symlink to point to the current release
        within_ssh("Creating current release symlink") do |ch|
          ch.exec "ln -s #{next_release_dir} next_tmp && mv -T next_tmp #{current_release_symlink}"
        end

        # create symlink in the web container to the current release if it doesn't exist
        unless is_symlink?(File.join(@server.web_dir, "forum"))
          within_ssh("Creating symlink in web directory") do |ch|
            ch.exec "ln -s #{current_release_symlink} #{@server.web_dir}/forum"
          end
        end
      end
    end

    def ssh_connection (&block)
      Net::SSH.start(@server.hostname, @server.username, :port => @server.ssh_port, :keys => [@key]) do |ssh|
        @ssh = ssh
        yield ssh
      end
    end

    private

    def next_release_dir
      File.join(@server.releases_dir, @timestamp.strftime(TIMESTAMP_DIRECTORY_FORMAT))
    end

    def current_release_symlink
      File.join(@server.releases_dir, "current")
    end

    def previous_release_symlink
      File.join(@server.releases_dir, "previous")
    end

    def is_symlink? (path)
      @ssh.exec!("[[ -h #{path} ]]; echo $?").chomp == "0"
    end

    def is_file? (path)
      @ssh.exec!("[[ -f #{path} ]]; echo $?").chomp == "0"
    end

    def is_directory? (path)
      @ssh.exec!("[[ -d #{path} ]]; echo $?").chomp == "0"
    end

    def exists? (path)
      @ssh.exec!("[[ -e #{path} ]]; echo $?").chomp == "0"
    end

    def within_ssh (msg, &block)
      channel = @ssh.open_channel do |ch|
        print "#{msg}..." if @debug
        yield ch

        ch.on_close { puts "done." if @debug }
 
        error_msg = ""
        ch.on_extended_data do |ch, type, data|
          next unless type == 1
          error_msg += data
        end

        ch.on_request("exit-status") do |ch, data|
          exit_code = data.read_long
          if exit_code > 0
            raise DeployerError, error_msg
          end
        end
      end
      channel.wait
    end

    class DeployerError < StandardError; end
  end
end
