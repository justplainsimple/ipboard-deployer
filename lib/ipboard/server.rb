module IPBoard
  class Server
    RELEASES_DIR_NAME = "releases"
    SHARED_DIR_NAME = "shared"

    attr_accessor :name, :hostname, :ssh_port, :username, :deploy_dir, :releases_dir, :shared_dir, :nfs_share_dir, :key, :web_dir

    # TODO releases_dir and shared_dir should exist in Deployer class
    def initialize (name, hostname, username, deploy_dir, nfs_share_dir, web_dir, ssh_port = 22)
      @name = name
      @hostname = hostname
      @username = username
      @deploy_dir = deploy_dir
      @nfs_share_dir = nfs_share_dir
      @ssh_port = ssh_port
      @web_dir = web_dir

      @releases_dir = File.join(@deploy_dir, RELEASES_DIR_NAME)
      @shared_dir = File.join(@deploy_dir, SHARED_DIR_NAME)
    end
  end
end
