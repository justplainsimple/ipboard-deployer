IPBoard Project

This project provides the following artifacts:
- a base install of IPBoard
- a deployment script for *nix systems
- a Vagrant provisioning system for local testing

For more information about Vagrant, please see http://vagrantup.com.

Directories

- base/
  - the base IP.Board software without any modifications (see base/version.txt for its current version).
- lib/
  - a set of libraries used by the deployment script.
- nfs/
  - a directory shared by Vagrant and the virtual machines in order to simulate a multi-node system.
- puppet/
  - a set of configuration scripts to provision/configure the virtual machines.
- deployer.rb
  - the deployment tool.
- deployer.yaml.sample
  - an example configuration file used by the deployment tool to configure itself.
- README.txt
  - this file you are reading.
- Vagrantfile
  - the configuration file that Vagrant uses to create virtual machines.

Installation Instructions

** Please note that you must have at least 1GB of RAM free in order to continue **

1.  Install VirtualBox (64-bit is best if your CPU supports it)
2.  Install Ruby 1.9.3
  - Windows: http://rubyinstaller.org/downloads/
    - When prompted, only select the option to update your PATH for the Ruby executables
3.  (If on Windows) Install Ruby 1.9.3 Development Kit
  - Instructions: http://github.com/oneclick/rubyinstaller/wiki/Development-Kit
4.  Install Vagrant gem
  - `gem install vagrant`
5.  Install SVN
6.  Export project from repository
7.  Open a command line application
8.  Enter project directory
9.  Run `vagrant status` to verify that the project's Vagrant configuration is working.
10. Run `vagrant box add precise32 http://files.vagrantup.com/precise32.box` to import the Ubuntu 12.04 VM image.
  - This will take several minutes (~300MB to download).
  - The box name "precise32" we save it as is important and will be used by the project.
11. Run `vagrant up`
  - This command creates and then starts the virtual machines
12. Copy deployer.yaml.sample into deployer.yaml
13. Change the values in deployer.yaml to suit your machine
14. Run `ruby deployer.rb`
  - This will take several minutes as the code is deployed and the system structure is set up.
15. Visit http://locahost/forum
16. Click through the Install Wizard
  - Requirements Page:
    - There should be no error on the Requirements page; 
    - Click Next.
  - EULA Page:
    - Agree to the License Agreement; 
    - Click Next
  - Applications Page:
    - Click Next.
  - Addresses Page:
    - Leave the fields with their default values; 
    - Click Next.
  - License Key Page:
    - Leave the license key field blank;
    - Click Next.
  - DB Details Page:
    - Enter the following details into the form:
      - SQL Host: 33.33.13.33
      - Database Name: ipboard
      - SQL Username: root
      - SQL Password: root
      - SQL Table Prefix:
      - MySQL Table Type: InnoDB
    - Click Next.
  - Admin Account Page:
    - Enter your information into the form.  Choose anything you like.
    - Click Next.
  - Installation Page:
    - Click "Start installation..."
    - Wait several minutes for the installation to finish.
  - Finished Page:
    - Close browser
17. Visit http://localhost/forum

The entire installation process should take around 34 minutes, depending on machine hardware configurations.  The time breakdown is as follows:
- Vagrant install/provision: 10 minutes
- Deployment: 20 minutes
- IPBoard install/configuration: 4 minutes  (manual step; dependent on user speed)

Uninstallation Instructions

** This section assumes that the installation succeeded **

1. Run `vagrant halt`
2. Run `vagrant destroy -f`
  - If you omit the "-f" flag, you will be prompted to destroy each VM.
3. Delete all subdirectories under nfs/

Your system has now returned to its original condition (with the exception of
Vagrant, VirtualBox, and Ruby being installed).

Stop/Start Instructions

** This section assumes that the installation succeeded **

In case you have installed the system, and now need to stop it so that you can
work on other projects, perform the following steps.

1. Run `vagrant halt`
2. Wait a few seconds for the system to shutdown.

Your system is now stopped.  To restart the system, perform the following 
steps.

1. Run `vagrant up`
2. Wait a few minutes for the system to start.

Your system will be started at the last point you left it.
